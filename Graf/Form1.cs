﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int n = (int)numericUpDown1.Value;
            dataGridView1.RowCount = n;
            for (int i=0; i < n; i++)
            {
                DataGridViewCheckBoxColumn column = new DataGridViewCheckBoxColumn();
                column.HeaderText = (i+1).ToString();
                column.ThreeState = false;
                dataGridView1.Columns.Insert(i, column);
                dataGridView1.Rows[i].HeaderCell.Value = (i+1).ToString();
                dataGridView1.Rows[i].Cells[i].ReadOnly = true;
                dataGridView1.Rows[i].Cells[i].Style.BackColor = Color.Red;
            }
            dataGridView1.Columns[n].Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool[,] matr = new bool[dataGridView1.RowCount, dataGridView1.RowCount];
            for (int i=0; i < dataGridView1.RowCount; i++)
            {
                for (int j=i+1;j< dataGridView1.RowCount; j++)
                {
                    if ((bool)dataGridView1.Rows[i].Cells[j].EditedFormattedValue)
                    {
                        matr[i, j] = true;
                    }
                    else
                    {
                        matr[i, j] = false;
                    }
                }
            }            
            Solver solver = new Solver(matr, dataGridView1.RowCount);
            ListEdge count = solver.Solve();
            MessageBox.Show(count.ToString(), "Длина кратчайшего цигкла = " + count.Length);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((bool)dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue)
                dataGridView1.Rows[e.ColumnIndex].Cells[e.RowIndex].Value = true;
            else
                dataGridView1.Rows[e.ColumnIndex].Cells[e.RowIndex].Value = false;
        }
    }
}
