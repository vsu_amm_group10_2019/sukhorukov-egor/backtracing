﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graf
{
    public class ListEdge
    {
        public List<int> Cycle;
        public int Length { get;}

        public ListEdge(List<int> cycle)
        {
            Cycle = cycle;
            Length = Cycle.Count;
        }
        public override string ToString()
        {
            string result = Cycle[0].ToString();
            for (int i = 1; i < Cycle.Count; i++)
            {
                result += '-' + Cycle[i].ToString();
            }
            return result;
        }
    }
    
}
