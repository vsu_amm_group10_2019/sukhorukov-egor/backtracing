﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graf
{
    public class Solver
    {
        private const int MinCycles = 4;
        public ListEdge Result;
        public List<Edge> E;
        private int Length { get; set; }
        public Solver (bool[,] matr, int n)
        {
            Result = null;
            Length = n;
            E = new List<Edge>();

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (matr[i, j])
                    {
                        E.Add(new Edge(i, j));
                    }
                }
            }
        }
        public ListEdge Solve()
        {
            CyclesSearch();
            return Result;
        }
        private void CyclesSearch()
        {
            bool[] visit = new bool[Length];
            for (int i = 0; i < Length; i++)
            {
                for (int k = 0; k < Length; k++)
                    visit[k] = true;
                List<int> cycle = new List<int>();
                cycle.Add(i + 1);
                DFScycle(i, i, E, visit, -1, cycle);
            }
        }

        
        private void DFScycle(int u, int endV, List<Edge> E, bool[] visit, int unavailableEdge, List<int> cycle)
        {
            if (u != endV)
                visit[u] = false;
            else if (cycle.Count >= MinCycles)
            {
                cycle.Reverse();
                if (Result == null || Result.Length > cycle.Count)
                {
                    Result = new ListEdge(cycle);                    
                    return;
                }                                
            }
            
            for (int w = 0; w < E.Count; w++)
            {
                if (w == unavailableEdge)
                    continue;
                List<int> cycleNEW = new List<int>(cycle);
                if (visit[E[w].v2] && E[w].v1 == u)
                {                    
                    cycleNEW.Add(E[w].v2 + 1);
                    DFScycle(E[w].v2, endV, E, visit, w, cycleNEW);
                    visit[E[w].v2] = true;                        
                }
                else if (visit[E[w].v1] && E[w].v2 == u)
                {                    
                    cycleNEW.Add(E[w].v1 + 1);
                    DFScycle(E[w].v1, endV, E, visit, w, cycleNEW);
                    visit[E[w].v1] = true;                   
                }
                if (cycleNEW.Count == MinCycles)
                {
                    return;
                }
            }
        }
    }
}
